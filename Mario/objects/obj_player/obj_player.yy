{
    "id": "262feaf7-53c9-4720-b8e9-4800f6866eae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "94d656dc-59b1-4a52-b302-1bbfcf9b6c15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "262feaf7-53c9-4720-b8e9-4800f6866eae"
        },
        {
            "id": "bb535e3b-ca0f-4ca3-82eb-7b8184193218",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "262feaf7-53c9-4720-b8e9-4800f6866eae"
        },
        {
            "id": "41642e2a-6081-43c9-b74a-10875bb29e5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "262feaf7-53c9-4720-b8e9-4800f6866eae"
        }
    ],
    "maskSpriteId": "a22b59a1-2448-4d52-b1a1-5a65ee9240cd",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a22b59a1-2448-4d52-b1a1-5a65ee9240cd",
    "visible": true
}