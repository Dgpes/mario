{
    "id": "a8335f5e-9f5f-4e03-bf54-0b206eab6350",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spawn",
    "eventList": [
        {
            "id": "b28fec53-5bc5-450a-8e52-429b9b917896",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "a8335f5e-9f5f-4e03-bf54-0b206eab6350"
        },
        {
            "id": "290559d3-c328-4fa3-9663-3e484be000fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "a8335f5e-9f5f-4e03-bf54-0b206eab6350"
        },
        {
            "id": "2202eb13-55e7-4ed6-bc13-0df0240b5cba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a8335f5e-9f5f-4e03-bf54-0b206eab6350"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a0007c54-a8e7-4fbd-a2bc-bb906eff45ee",
    "visible": true
}