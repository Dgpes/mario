{
    "id": "e33198c1-e06a-45d4-a8c0-470ad4181b0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy2",
    "eventList": [
        {
            "id": "aeb6e3e5-73ac-4a7c-8aef-d98b268c64d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e33198c1-e06a-45d4-a8c0-470ad4181b0c"
        },
        {
            "id": "9cc4d4b6-8734-4bcb-9130-12f443bb4989",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e33198c1-e06a-45d4-a8c0-470ad4181b0c"
        },
        {
            "id": "c830403c-c276-41b5-9f28-7ab07f9dd186",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1bd82edc-b8c6-4a4d-940d-03ce5af88e79",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e33198c1-e06a-45d4-a8c0-470ad4181b0c"
        },
        {
            "id": "fb88ec7b-b7c8-4cc4-9360-57e8e7ccf595",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "e33198c1-e06a-45d4-a8c0-470ad4181b0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dd190580-6539-42d9-8268-d4234dab380e",
    "visible": true
}