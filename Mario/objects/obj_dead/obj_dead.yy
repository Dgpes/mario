{
    "id": "0ce99d23-6f2d-4262-9e45-daa8e61d2698",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dead",
    "eventList": [
        {
            "id": "66f468cf-6f61-4e5d-96a3-e5248b5df50b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0ce99d23-6f2d-4262-9e45-daa8e61d2698"
        },
        {
            "id": "44dca242-e116-4881-a604-17836a5cb88b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0ce99d23-6f2d-4262-9e45-daa8e61d2698"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "432b25b5-fb06-470a-a84a-41db87249e82",
    "visible": true
}