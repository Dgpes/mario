/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 0B14FC57
/// @DnDArgument : "code" "key_right = keyboard_check(vk_right);$(13_10)key_left = -keyboard_check(vk_left);$(13_10)key_jump = keyboard_check_pressed(vk_space);$(13_10)$(13_10)$(13_10)move = key_left + key_right;$(13_10)hsp = move*movespeed;$(13_10) $(13_10)if(vsp<10)vsp+=grav;$(13_10) $(13_10)if(place_meeting(x,y+1,object_suelo))$(13_10){$(13_10)    //Check if recently grounded$(13_10)    if(!grounded && !key_jump){$(13_10)        hkp_count = 0; //Init horizontal count$(13_10)        jumping = false;$(13_10)    }else if(grounded && key_jump){ //recently jumping$(13_10)        jumping = true;$(13_10)    }$(13_10) $(13_10)    //Check if player grounded$(13_10)    grounded = !key_jump;$(13_10)   $(13_10)    vsp = key_jump * -jumpspeed;$(13_10)}else if (place_meeting(x,y+1,object_suelo2)){$(13_10)	//Check if recently grounded$(13_10)    if(!grounded && !key_jump){$(13_10)        hkp_count = 0; //Init horizontal count$(13_10)        jumping = false;$(13_10)    }else if(grounded && key_jump){ //recently jumping$(13_10)        jumping = true;$(13_10)    }$(13_10) $(13_10)    //Check if player grounded$(13_10)    grounded = !key_jump;$(13_10)   $(13_10)    vsp = key_jump * -jumpspeed;$(13_10)}$(13_10) $(13_10)//Init hsp_jump_applied$(13_10)if(grounded){$(13_10)    hsp_jump_applied = 0;$(13_10)}$(13_10) $(13_10)//Check horizontal counts$(13_10)if(move!=0 && grounded){$(13_10) hkp_count++;$(13_10)}else if(move==0 && grounded){$(13_10) hkp_count=0;$(13_10)}$(13_10) $(13_10)$(13_10)$(13_10)//Check jumping$(13_10)if(jumping){$(13_10)   $(13_10)    //check if previously we have jump$(13_10)    if(hsp_jump_applied == 0){$(13_10)        hsp_jump_applied = sign(move);      $(13_10)    }$(13_10) $(13_10)    //don't jump horizontal$(13_10)    if(hkp_count < hkp_count_small ){$(13_10)        hsp = 0;$(13_10)    }else if(hkp_count >= hkp_count_small && hkp_count < hkp_count_big){ //small jump$(13_10)        hsp = hsp_jump_applied * hsp_jump_constant_small;$(13_10)    }else{ // big jump$(13_10)        hsp = hsp_jump_applied *hsp_jump_constant_big$(13_10)    }$(13_10)}$(13_10) $(13_10)//horizontal collision$(13_10)if(place_meeting(x+hsp,y,object_suelo))$(13_10){$(13_10)    while(!place_meeting(x+sign(hsp),y,object_suelo))$(13_10)    {$(13_10)        x+= sign(hsp);$(13_10)    }$(13_10)    hsp = 0;$(13_10)}else if(place_meeting(x+hsp,y,object_suelo2)){$(13_10)	while(!place_meeting(x+sign(hsp),y,object_suelo2))$(13_10)    {$(13_10)        x+= sign(hsp);$(13_10)    }$(13_10)    hsp = 0;$(13_10)}$(13_10) $(13_10)x+= hsp;$(13_10) $(13_10)//vertical collision$(13_10)if(place_meeting(x,y+vsp,object_suelo))$(13_10){$(13_10)    while(!place_meeting(x, sign(vsp) + y,object_suelo))$(13_10)    {$(13_10)        y+= sign(vsp);$(13_10)    }$(13_10)    vsp = 0;$(13_10)}else if (place_meeting(x,y+vsp,object_suelo2)){$(13_10)	while(!place_meeting(x, sign(vsp) + y,object_suelo2))$(13_10)    {$(13_10)        y+= sign(vsp);$(13_10)    }$(13_10)    vsp = 0;$(13_10)}$(13_10) $(13_10)y+= vsp;$(13_10)/*$(13_10)if(key_right && key_left){$(13_10)	key_right = false;$(13_10)	key_left = false;$(13_10)}$(13_10)/*$(13_10)if(dir_left){$(13_10)	ent_curr_dir = vk_left;$(13_10)}else if(dir_right){$(13_10)	ent_curr_dir = vk_right;$(13_10)}$(13_10)*/$(13_10)if(grounded){$(13_10)	if(hsp==0){$(13_10)		if(sprite_index!= sp_idle) image_index = 0;$(13_10)		sprite_index = sp_idle;$(13_10)	}else if(keyboard_check(key_right)) {$(13_10)			sprite_index = sp_walking_right;$(13_10)		}else {$(13_10)			sprite_index = sp_walking_left;$(13_10)		}$(13_10)	$(13_10)}$(13_10)else if(vsp<0){$(13_10)		if(keyboard_check(key_right)){$(13_10)			sprite_index = sp_jump_right;$(13_10)		}$(13_10)		else {$(13_10)			sprite_index = sp_jump_left;$(13_10)		}$(13_10)	}$(13_10)	else {$(13_10)		if(keyboard_check(key_right)){$(13_10)			sprite_index = sp_jump_right;$(13_10)		}$(13_10)		else {$(13_10)			sprite_index = sp_jump_left;$(13_10)		}$(13_10)	}$(13_10)$(13_10)$(13_10)$(13_10)"
key_right = keyboard_check(vk_right);
key_left = -keyboard_check(vk_left);
key_jump = keyboard_check_pressed(vk_space);


move = key_left + key_right;
hsp = move*movespeed;
 
if(vsp<10)vsp+=grav;
 
if(place_meeting(x,y+1,object_suelo))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }
 
    //Check if player grounded
    grounded = !key_jump;
   
    vsp = key_jump * -jumpspeed;
}else if (place_meeting(x,y+1,object_suelo2)){
	//Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }
 
    //Check if player grounded
    grounded = !key_jump;
   
    vsp = key_jump * -jumpspeed;
}
 
//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}
 
//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 hkp_count=0;
}
 


//Check jumping
if(jumping){
   
    //check if previously we have jump
    if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);      
    }
 
    //don't jump horizontal
    if(hkp_count < hkp_count_small ){
        hsp = 0;
    }else if(hkp_count >= hkp_count_small && hkp_count < hkp_count_big){ //small jump
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }else{ // big jump
        hsp = hsp_jump_applied *hsp_jump_constant_big
    }
}
 
//horizontal collision
if(place_meeting(x+hsp,y,object_suelo))
{
    while(!place_meeting(x+sign(hsp),y,object_suelo))
    {
        x+= sign(hsp);
    }
    hsp = 0;
}else if(place_meeting(x+hsp,y,object_suelo2)){
	while(!place_meeting(x+sign(hsp),y,object_suelo2))
    {
        x+= sign(hsp);
    }
    hsp = 0;
}
 
x+= hsp;
 
//vertical collision
if(place_meeting(x,y+vsp,object_suelo))
{
    while(!place_meeting(x, sign(vsp) + y,object_suelo))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}else if (place_meeting(x,y+vsp,object_suelo2)){
	while(!place_meeting(x, sign(vsp) + y,object_suelo2))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}
 
y+= vsp;
/*
if(key_right && key_left){
	key_right = false;
	key_left = false;
}
/*
if(dir_left){
	ent_curr_dir = vk_left;
}else if(dir_right){
	ent_curr_dir = vk_right;
}
*/
if(grounded){
	if(hsp==0){
		if(sprite_index!= sp_idle) image_index = 0;
		sprite_index = sp_idle;
	}else if(keyboard_check(key_right)) {
			sprite_index = sp_walking_right;
		}else {
			sprite_index = sp_walking_left;
		}
	
}
else if(vsp<0){
		if(keyboard_check(key_right)){
			sprite_index = sp_jump_right;
		}
		else {
			sprite_index = sp_jump_left;
		}
	}
	else {
		if(keyboard_check(key_right)){
			sprite_index = sp_jump_right;
		}
		else {
			sprite_index = sp_jump_left;
		}
	}



/**/