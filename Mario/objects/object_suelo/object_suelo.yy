{
    "id": "b57346b8-789f-4083-b7b1-a93ff0643bcd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_suelo",
    "eventList": [
        
    ],
    "maskSpriteId": "6c4e179e-f5da-4062-ba3b-f955c792f4e6",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6c4e179e-f5da-4062-ba3b-f955c792f4e6",
    "visible": true
}