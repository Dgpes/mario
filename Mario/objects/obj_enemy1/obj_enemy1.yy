{
    "id": "6cf79d18-1987-4480-85ff-2cac729f9e41",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy1",
    "eventList": [
        {
            "id": "b181c9b6-8e7f-4212-90b5-fb72c0fac1c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6cf79d18-1987-4480-85ff-2cac729f9e41"
        },
        {
            "id": "c8a73cf0-39fb-4ff6-be7f-990a6b857170",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6cf79d18-1987-4480-85ff-2cac729f9e41"
        },
        {
            "id": "9a33dca3-1cc5-43d2-ba3e-bb4e426347a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1bd82edc-b8c6-4a4d-940d-03ce5af88e79",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6cf79d18-1987-4480-85ff-2cac729f9e41"
        },
        {
            "id": "c3a4f719-555a-4d21-9532-1cc19229db4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "6cf79d18-1987-4480-85ff-2cac729f9e41"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "48d2f83d-0404-4e8a-9b26-2776757bb137",
    "visible": true
}