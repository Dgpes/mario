{
    "id": "6627e1ec-6cd6-40de-b2e2-4d4b970b6024",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spawn1",
    "eventList": [
        {
            "id": "e08d926d-97d7-45ba-a0b4-ce76e84391bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "6627e1ec-6cd6-40de-b2e2-4d4b970b6024"
        },
        {
            "id": "f39a010b-df1f-4ab7-bd97-ea83b67f8003",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "6627e1ec-6cd6-40de-b2e2-4d4b970b6024"
        },
        {
            "id": "3fd289df-0a63-4d6c-acc2-6255286f7621",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6627e1ec-6cd6-40de-b2e2-4d4b970b6024"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a0007c54-a8e7-4fbd-a2bc-bb906eff45ee",
    "visible": true
}