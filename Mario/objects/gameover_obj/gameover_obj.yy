{
    "id": "2c8fc443-8a89-490a-951f-4f1be7f29295",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "gameover_obj",
    "eventList": [
        {
            "id": "7e4b6e35-53d9-444a-a56c-acccafda75d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "2c8fc443-8a89-490a-951f-4f1be7f29295"
        },
        {
            "id": "6f3c703e-5da1-43dc-b224-3417fade10a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2c8fc443-8a89-490a-951f-4f1be7f29295"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f78f09de-0d40-4b54-aedc-c59e8c0b5217",
    "visible": true
}