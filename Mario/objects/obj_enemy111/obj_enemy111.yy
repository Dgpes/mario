{
    "id": "e8a3d4c4-9ec5-4983-aba9-e49ac569c3df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy111",
    "eventList": [
        {
            "id": "266b794c-29be-482a-aa39-187ff5598a51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e8a3d4c4-9ec5-4983-aba9-e49ac569c3df"
        },
        {
            "id": "15448257-a868-4155-9d9f-19f796fb7965",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e8a3d4c4-9ec5-4983-aba9-e49ac569c3df"
        },
        {
            "id": "ebeef165-a564-4740-8e74-f244ae415253",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1bd82edc-b8c6-4a4d-940d-03ce5af88e79",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e8a3d4c4-9ec5-4983-aba9-e49ac569c3df"
        },
        {
            "id": "821908fb-920a-46c5-adcd-a6dc79105fe7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "e8a3d4c4-9ec5-4983-aba9-e49ac569c3df"
        },
        {
            "id": "e68c5b72-0531-495f-8842-245be327032b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "79663e5d-5e27-44df-adc7-fc899d9ac22d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e8a3d4c4-9ec5-4983-aba9-e49ac569c3df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6311a902-8139-4633-87d4-378d69751342",
    "visible": true
}