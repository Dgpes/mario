{
    "id": "1bd82edc-b8c6-4a4d-940d-03ce5af88e79",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_suelo2",
    "eventList": [
        {
            "id": "a05a6b88-bd1f-4415-baf7-d003d75283d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1bd82edc-b8c6-4a4d-940d-03ce5af88e79"
        },
        {
            "id": "85caeabc-3dbb-4e4a-af77-40635e0b0625",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1bd82edc-b8c6-4a4d-940d-03ce5af88e79"
        },
        {
            "id": "4a7f0e2d-5ac0-4e59-aa8d-3ae77cd8271d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1bd82edc-b8c6-4a4d-940d-03ce5af88e79"
        }
    ],
    "maskSpriteId": "eb677e99-4530-4488-b69a-015cef421430",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "eb677e99-4530-4488-b69a-015cef421430",
    "visible": true
}