{
    "id": "1634374c-9488-4bdb-8692-c2f5b140fe6d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "b8f79b1a-822d-4674-947b-887e01757558",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1634374c-9488-4bdb-8692-c2f5b140fe6d"
        },
        {
            "id": "4664d3da-a609-45b1-85ab-28ed96e8340a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1634374c-9488-4bdb-8692-c2f5b140fe6d"
        },
        {
            "id": "393f0ba5-1b26-4391-8552-dee419b44d73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1bd82edc-b8c6-4a4d-940d-03ce5af88e79",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1634374c-9488-4bdb-8692-c2f5b140fe6d"
        },
        {
            "id": "85d4c80a-e208-47be-80ce-6ce0a2dab4b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "1634374c-9488-4bdb-8692-c2f5b140fe6d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6311a902-8139-4633-87d4-378d69751342",
    "visible": true
}