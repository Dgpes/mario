{
    "id": "02870ded-d9ce-4ac4-af16-a1ccee72ddcb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button",
    "eventList": [
        {
            "id": "6ebc6ef7-34ba-4171-a753-d4c8f33fdbaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02870ded-d9ce-4ac4-af16-a1ccee72ddcb"
        },
        {
            "id": "bde5cb90-d1cf-4ee9-a4d3-297b7f59bd5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "02870ded-d9ce-4ac4-af16-a1ccee72ddcb"
        },
        {
            "id": "d1ca7c83-6dd6-4298-bc48-6f75d144e422",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "02870ded-d9ce-4ac4-af16-a1ccee72ddcb"
        },
        {
            "id": "ef837669-9e3c-4ab4-a369-d3195a6cf95c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "02870ded-d9ce-4ac4-af16-a1ccee72ddcb"
        },
        {
            "id": "a6d4563e-528d-4b06-b5b0-e78606f24868",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "02870ded-d9ce-4ac4-af16-a1ccee72ddcb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2f221846-87b2-430f-8f81-6e7314f221c8",
    "visible": true
}