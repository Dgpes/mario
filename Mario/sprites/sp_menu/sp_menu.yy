{
    "id": "1c9eecc6-fa5b-40c6-9214-498547e17229",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 253,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac8007cc-5734-40fc-833b-e495b107518f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9eecc6-fa5b-40c6-9214-498547e17229",
            "compositeImage": {
                "id": "2e02275b-cbf3-4770-913a-06a111f8d0bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac8007cc-5734-40fc-833b-e495b107518f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6efeff59-dad6-4db8-b363-4be184af05f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac8007cc-5734-40fc-833b-e495b107518f",
                    "LayerId": "31218859-fe3f-42cc-af0e-0e0664883048"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "31218859-fe3f-42cc-af0e-0e0664883048",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c9eecc6-fa5b-40c6-9214-498547e17229",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 254,
    "xorig": 0,
    "yorig": 0
}