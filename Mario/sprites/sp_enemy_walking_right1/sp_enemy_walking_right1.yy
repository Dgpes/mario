{
    "id": "dd190580-6539-42d9-8268-d4234dab380e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_enemy_walking_right1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db872d9d-68f6-440d-ab20-6da0c2201a4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd190580-6539-42d9-8268-d4234dab380e",
            "compositeImage": {
                "id": "914e001f-5909-483a-a778-11bb1ed009b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db872d9d-68f6-440d-ab20-6da0c2201a4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8923dc2-384d-49a4-a2a8-1032aa8894c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db872d9d-68f6-440d-ab20-6da0c2201a4a",
                    "LayerId": "f7b0304c-3da5-4497-b78d-3e62b31a6c09"
                }
            ]
        },
        {
            "id": "96a6145b-0dc0-4f06-8fc4-9692d8484e62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd190580-6539-42d9-8268-d4234dab380e",
            "compositeImage": {
                "id": "b840d49f-f368-4ef6-af4e-b3553a48fedd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96a6145b-0dc0-4f06-8fc4-9692d8484e62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c304736d-af5f-4964-a8a0-2800348bb848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96a6145b-0dc0-4f06-8fc4-9692d8484e62",
                    "LayerId": "f7b0304c-3da5-4497-b78d-3e62b31a6c09"
                }
            ]
        },
        {
            "id": "958d4598-07be-408a-ac86-950326d1c158",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd190580-6539-42d9-8268-d4234dab380e",
            "compositeImage": {
                "id": "7be9e8df-7315-4274-ba85-67e78956ba10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "958d4598-07be-408a-ac86-950326d1c158",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "296c06f8-7fb3-4edc-a496-5c49ec60dbca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "958d4598-07be-408a-ac86-950326d1c158",
                    "LayerId": "f7b0304c-3da5-4497-b78d-3e62b31a6c09"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f7b0304c-3da5-4497-b78d-3e62b31a6c09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd190580-6539-42d9-8268-d4234dab380e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 9
}