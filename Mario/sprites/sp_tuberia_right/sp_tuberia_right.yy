{
    "id": "f6e47ca7-5a81-4df6-95da-782062e9e1e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_tuberia_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ac130a0-8626-4be4-8fde-75c4533c8fe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6e47ca7-5a81-4df6-95da-782062e9e1e4",
            "compositeImage": {
                "id": "18992e22-6df0-46a7-8d3b-8cb3c3bf933f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ac130a0-8626-4be4-8fde-75c4533c8fe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e0cdcb4-d355-4aa9-b851-91965c62cb15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ac130a0-8626-4be4-8fde-75c4533c8fe5",
                    "LayerId": "a8a20763-e378-46a8-b9c5-1347a8df9b9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a8a20763-e378-46a8-b9c5-1347a8df9b9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6e47ca7-5a81-4df6-95da-782062e9e1e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 16
}