{
    "id": "432b25b5-fb06-470a-a84a-41db87249e82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffc290c4-08c4-4d47-96ca-8148f6035a61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "432b25b5-fb06-470a-a84a-41db87249e82",
            "compositeImage": {
                "id": "dc5fa42d-3d1b-4800-8bc0-8fba44508203",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffc290c4-08c4-4d47-96ca-8148f6035a61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1a40eb6-bb42-4979-9000-653bf9a2197a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffc290c4-08c4-4d47-96ca-8148f6035a61",
                    "LayerId": "cc0cf054-749c-4a30-b8ae-8db91f8ea498"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "cc0cf054-749c-4a30-b8ae-8db91f8ea498",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "432b25b5-fb06-470a-a84a-41db87249e82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 10
}