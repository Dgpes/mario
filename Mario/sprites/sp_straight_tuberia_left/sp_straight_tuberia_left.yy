{
    "id": "4dc2a5b4-c1b5-4903-9549-916fa6f6f5bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_straight_tuberia_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2066821a-79ca-4ec6-b2df-47a3522b81f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dc2a5b4-c1b5-4903-9549-916fa6f6f5bb",
            "compositeImage": {
                "id": "3cffda5d-ac76-4c39-8b96-db5b300e2981",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2066821a-79ca-4ec6-b2df-47a3522b81f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6261b73d-4b37-4a78-b62c-c0c93699e973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2066821a-79ca-4ec6-b2df-47a3522b81f2",
                    "LayerId": "904f1232-2772-4187-99f3-d47883ab495d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "904f1232-2772-4187-99f3-d47883ab495d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4dc2a5b4-c1b5-4903-9549-916fa6f6f5bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 17,
    "yorig": 11
}