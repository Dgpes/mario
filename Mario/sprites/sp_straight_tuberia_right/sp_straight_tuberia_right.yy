{
    "id": "752fa0fd-97db-4f51-87fa-139a44c47076",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_straight_tuberia_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 15,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6733e6e3-4e16-4d09-944f-aed7bea389ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "752fa0fd-97db-4f51-87fa-139a44c47076",
            "compositeImage": {
                "id": "b61e0ee7-37e3-4f43-a2ba-c7654c87c036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6733e6e3-4e16-4d09-944f-aed7bea389ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "719aba09-2ad8-46a4-8f6e-1f664898ac4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6733e6e3-4e16-4d09-944f-aed7bea389ba",
                    "LayerId": "6dd7be42-49f6-4df9-a628-ad10b9be03ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "6dd7be42-49f6-4df9-a628-ad10b9be03ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "752fa0fd-97db-4f51-87fa-139a44c47076",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 11
}