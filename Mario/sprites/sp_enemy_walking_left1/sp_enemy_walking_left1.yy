{
    "id": "46a3a0e7-42fb-4136-81e5-0c330c3c3230",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_enemy_walking_left1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d64bc64-a1ff-4375-bfcc-f2514d099965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46a3a0e7-42fb-4136-81e5-0c330c3c3230",
            "compositeImage": {
                "id": "5f2a003b-cabe-4a9f-88a3-a01f6e18d457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d64bc64-a1ff-4375-bfcc-f2514d099965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76e6e447-c417-465a-8f08-0b7cc0f6452d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d64bc64-a1ff-4375-bfcc-f2514d099965",
                    "LayerId": "5f259890-f0cf-40a8-beb2-6f5dd5f1a975"
                }
            ]
        },
        {
            "id": "bc46cee0-9828-4332-9d9a-98fb3e1200c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46a3a0e7-42fb-4136-81e5-0c330c3c3230",
            "compositeImage": {
                "id": "f1451c3e-e46d-4abd-856b-66fa4a51fa42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc46cee0-9828-4332-9d9a-98fb3e1200c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f0eafb3-a2e8-4781-a65d-4ceea778e6fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc46cee0-9828-4332-9d9a-98fb3e1200c6",
                    "LayerId": "5f259890-f0cf-40a8-beb2-6f5dd5f1a975"
                }
            ]
        },
        {
            "id": "074ab266-0905-4068-bb51-3201f66e5d8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46a3a0e7-42fb-4136-81e5-0c330c3c3230",
            "compositeImage": {
                "id": "795203f2-6b48-4bcb-b2c4-905528de6110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "074ab266-0905-4068-bb51-3201f66e5d8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3446fa6a-b8e9-44e3-85a6-eec4375704e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "074ab266-0905-4068-bb51-3201f66e5d8d",
                    "LayerId": "5f259890-f0cf-40a8-beb2-6f5dd5f1a975"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5f259890-f0cf-40a8-beb2-6f5dd5f1a975",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46a3a0e7-42fb-4136-81e5-0c330c3c3230",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 9
}