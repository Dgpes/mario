{
    "id": "aae02768-599f-47a4-b09b-7b1a346af699",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_jump_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "760edbad-ca6b-419a-b7e2-4a33b3a15693",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aae02768-599f-47a4-b09b-7b1a346af699",
            "compositeImage": {
                "id": "37e3f1f0-970d-4f22-9785-ee9cd8156d4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760edbad-ca6b-419a-b7e2-4a33b3a15693",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad57cb1-9710-4d94-91a8-4e6eca403eeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760edbad-ca6b-419a-b7e2-4a33b3a15693",
                    "LayerId": "0f8a5aef-fdb8-4d07-aa58-7ad064cfac20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "0f8a5aef-fdb8-4d07-aa58-7ad064cfac20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aae02768-599f-47a4-b09b-7b1a346af699",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 11
}