{
    "id": "f78f09de-0d40-4b54-aedc-c59e8c0b5217",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gameover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d768efa7-6abd-4dd9-af83-d37fa7009030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f78f09de-0d40-4b54-aedc-c59e8c0b5217",
            "compositeImage": {
                "id": "d1dd9a59-9bb8-4aa5-b8f5-bb9b96446fa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d768efa7-6abd-4dd9-af83-d37fa7009030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a340128b-4596-4420-b73b-dd36a079d6f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d768efa7-6abd-4dd9-af83-d37fa7009030",
                    "LayerId": "0a642de3-1c3d-47f6-9650-870cb0e744fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 75,
    "layers": [
        {
            "id": "0a642de3-1c3d-47f6-9650-870cb0e744fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f78f09de-0d40-4b54-aedc-c59e8c0b5217",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 0,
    "yorig": 0
}