{
    "id": "ff98c4b0-f7a8-4bd2-9420-228469338a6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite26",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 15,
    "bbox_right": 96,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "173700cc-d5f1-463e-9fab-28cf00ce4e11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff98c4b0-f7a8-4bd2-9420-228469338a6f",
            "compositeImage": {
                "id": "7e618ddd-d546-4be2-a978-01d369b68492",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "173700cc-d5f1-463e-9fab-28cf00ce4e11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff7e566-11cd-40f0-9c16-25e4ef515830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "173700cc-d5f1-463e-9fab-28cf00ce4e11",
                    "LayerId": "0dc64824-a46b-49b7-9c1d-1f6c2c0dc030"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0dc64824-a46b-49b7-9c1d-1f6c2c0dc030",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff98c4b0-f7a8-4bd2-9420-228469338a6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}