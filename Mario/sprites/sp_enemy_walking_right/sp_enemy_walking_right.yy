{
    "id": "6311a902-8139-4633-87d4-378d69751342",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_enemy_walking_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffc70ddd-eded-4a99-8dfa-3be9a1e15f32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6311a902-8139-4633-87d4-378d69751342",
            "compositeImage": {
                "id": "621f398d-a921-42a2-81de-f430b6fbbc95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffc70ddd-eded-4a99-8dfa-3be9a1e15f32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbd74df4-56c7-457a-812e-0088cd3b9190",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffc70ddd-eded-4a99-8dfa-3be9a1e15f32",
                    "LayerId": "ade68a55-6d60-477c-af36-b51c754f4628"
                }
            ]
        },
        {
            "id": "8dc4988f-c3a8-4e2c-8b34-2cfcc0e2a17b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6311a902-8139-4633-87d4-378d69751342",
            "compositeImage": {
                "id": "b07d2747-798b-426e-a42b-3005151e2f56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dc4988f-c3a8-4e2c-8b34-2cfcc0e2a17b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97109783-3427-4c3c-a0bf-f1a6e54ab569",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dc4988f-c3a8-4e2c-8b34-2cfcc0e2a17b",
                    "LayerId": "ade68a55-6d60-477c-af36-b51c754f4628"
                }
            ]
        },
        {
            "id": "e0ddb6dd-9045-4944-911f-76538a0900b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6311a902-8139-4633-87d4-378d69751342",
            "compositeImage": {
                "id": "56e63e0f-b9cc-4bb3-a950-2f87002c30cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0ddb6dd-9045-4944-911f-76538a0900b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "084ad306-d7c6-4c20-b7cb-d3a3906270a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0ddb6dd-9045-4944-911f-76538a0900b4",
                    "LayerId": "ade68a55-6d60-477c-af36-b51c754f4628"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ade68a55-6d60-477c-af36-b51c754f4628",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6311a902-8139-4633-87d4-378d69751342",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 9
}