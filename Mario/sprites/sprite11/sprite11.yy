{
    "id": "eb677e99-4530-4488-b69a-015cef421430",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6716f9a-384a-4769-9a32-326ebe1f0846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb677e99-4530-4488-b69a-015cef421430",
            "compositeImage": {
                "id": "4a90f2f6-960b-4bca-bbca-74027e32e83a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6716f9a-384a-4769-9a32-326ebe1f0846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0edcd5de-1eef-40ca-9d67-ab25e857c204",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6716f9a-384a-4769-9a32-326ebe1f0846",
                    "LayerId": "0c694f52-c677-4bc2-933c-20f2a28cc13b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "0c694f52-c677-4bc2-933c-20f2a28cc13b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb677e99-4530-4488-b69a-015cef421430",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 7
}