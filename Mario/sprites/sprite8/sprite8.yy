{
    "id": "4d7d557e-6ade-400d-885c-daec11ab5f3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 672,
    "bbox_left": 0,
    "bbox_right": 435,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba1a2552-60b6-48c0-8d4a-7393447f5ec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d7d557e-6ade-400d-885c-daec11ab5f3b",
            "compositeImage": {
                "id": "99e5f990-cd51-417e-bd58-a1e931cc2693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba1a2552-60b6-48c0-8d4a-7393447f5ec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa11aa8e-6a63-4890-8648-1d15e9a7b5c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba1a2552-60b6-48c0-8d4a-7393447f5ec7",
                    "LayerId": "9f06db3f-7677-4f03-8c98-4f76c6cfebda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 673,
    "layers": [
        {
            "id": "9f06db3f-7677-4f03-8c98-4f76c6cfebda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d7d557e-6ade-400d-885c-daec11ab5f3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 436,
    "xorig": 222,
    "yorig": 341
}