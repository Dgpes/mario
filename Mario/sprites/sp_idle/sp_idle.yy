{
    "id": "a22b59a1-2448-4d52-b1a1-5a65ee9240cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 3,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb95a50e-a4e8-47c8-a91f-fc66f651a251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a22b59a1-2448-4d52-b1a1-5a65ee9240cd",
            "compositeImage": {
                "id": "a03231d4-83cc-4902-8f85-e8048479b34c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb95a50e-a4e8-47c8-a91f-fc66f651a251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41b87c65-9084-4ff5-ae24-99eec630322b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb95a50e-a4e8-47c8-a91f-fc66f651a251",
                    "LayerId": "c4571f54-d718-4591-ab55-ca760a68ec43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "c4571f54-d718-4591-ab55-ca760a68ec43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a22b59a1-2448-4d52-b1a1-5a65ee9240cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 12
}