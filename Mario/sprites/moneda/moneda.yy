{
    "id": "5f85657e-dc87-41c6-a058-5e326bd46f16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "moneda",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe39fdc7-1563-444b-ab0f-e060e2edb70b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f85657e-dc87-41c6-a058-5e326bd46f16",
            "compositeImage": {
                "id": "5ba9e93a-ab27-4bcc-b985-7f2520d1e7b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe39fdc7-1563-444b-ab0f-e060e2edb70b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "968ec2b5-60b8-4494-a9f1-6219ccb05053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe39fdc7-1563-444b-ab0f-e060e2edb70b",
                    "LayerId": "f59fc675-8c2b-4338-a8ec-19119455d6f9"
                }
            ]
        },
        {
            "id": "932c632b-0089-490f-ad1c-1f8258ed03db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f85657e-dc87-41c6-a058-5e326bd46f16",
            "compositeImage": {
                "id": "bacc1df2-b362-4913-b66d-a7102dd342b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "932c632b-0089-490f-ad1c-1f8258ed03db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f29949ec-f358-4ac8-b467-c0c62d139abc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "932c632b-0089-490f-ad1c-1f8258ed03db",
                    "LayerId": "f59fc675-8c2b-4338-a8ec-19119455d6f9"
                }
            ]
        },
        {
            "id": "9a5d0fb3-8700-4d63-82ee-128df1750a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f85657e-dc87-41c6-a058-5e326bd46f16",
            "compositeImage": {
                "id": "951bcc9d-349e-4e80-9381-5cd395bea15a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a5d0fb3-8700-4d63-82ee-128df1750a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aeb58db-79a0-48ff-8912-e141842aea14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a5d0fb3-8700-4d63-82ee-128df1750a38",
                    "LayerId": "f59fc675-8c2b-4338-a8ec-19119455d6f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f59fc675-8c2b-4338-a8ec-19119455d6f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f85657e-dc87-41c6-a058-5e326bd46f16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}