{
    "id": "d434dc39-a9ff-4570-9535-b95d795845b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_walking_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba043cde-783d-4c8d-99b2-799445dd33ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d434dc39-a9ff-4570-9535-b95d795845b4",
            "compositeImage": {
                "id": "0046b45a-197d-4980-9164-08aa6f2a05f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba043cde-783d-4c8d-99b2-799445dd33ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f772976-5508-47bf-83ef-02947bfcafd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba043cde-783d-4c8d-99b2-799445dd33ad",
                    "LayerId": "1c07f164-7775-45b8-aa1c-6a2db0abf322"
                }
            ]
        },
        {
            "id": "4fb9712c-4252-44ab-82ee-7c25ee7156a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d434dc39-a9ff-4570-9535-b95d795845b4",
            "compositeImage": {
                "id": "fb2f898f-ae48-44b8-86ab-5a2e20c82d94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb9712c-4252-44ab-82ee-7c25ee7156a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5cfc9f5-98de-484e-b4de-b11bd826a422",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb9712c-4252-44ab-82ee-7c25ee7156a3",
                    "LayerId": "1c07f164-7775-45b8-aa1c-6a2db0abf322"
                }
            ]
        },
        {
            "id": "aa963f1b-dd34-40e0-90f8-44cf8a5d1c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d434dc39-a9ff-4570-9535-b95d795845b4",
            "compositeImage": {
                "id": "a7b99028-7f0b-48c5-b37b-1f431f5553a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa963f1b-dd34-40e0-90f8-44cf8a5d1c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b3360dd-0a20-4030-96f9-8a0148231327",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa963f1b-dd34-40e0-90f8-44cf8a5d1c05",
                    "LayerId": "1c07f164-7775-45b8-aa1c-6a2db0abf322"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "1c07f164-7775-45b8-aa1c-6a2db0abf322",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d434dc39-a9ff-4570-9535-b95d795845b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}