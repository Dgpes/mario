{
    "id": "88581923-9133-416e-9f45-78ddc7c53296",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_walking_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d13baed6-1383-4af2-9c8e-23d5b6772b33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88581923-9133-416e-9f45-78ddc7c53296",
            "compositeImage": {
                "id": "61244da4-cff2-4ad9-bc19-af18db703cd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d13baed6-1383-4af2-9c8e-23d5b6772b33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cba061f-c24e-4956-88c6-e48374499f17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d13baed6-1383-4af2-9c8e-23d5b6772b33",
                    "LayerId": "a114fcab-3b29-434e-a43b-ff3a56a7e634"
                }
            ]
        },
        {
            "id": "f52d3f47-44c6-482b-8a9b-67cba54946a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88581923-9133-416e-9f45-78ddc7c53296",
            "compositeImage": {
                "id": "763927d7-4b3e-4dc1-b33e-d569ca7df766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f52d3f47-44c6-482b-8a9b-67cba54946a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e102b528-bda6-4b23-8aa9-dda32e046207",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f52d3f47-44c6-482b-8a9b-67cba54946a0",
                    "LayerId": "a114fcab-3b29-434e-a43b-ff3a56a7e634"
                }
            ]
        },
        {
            "id": "d6aa6dd8-e2b1-4efb-adcc-26fd8281cb53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88581923-9133-416e-9f45-78ddc7c53296",
            "compositeImage": {
                "id": "83018d3e-80bf-49ec-b3be-6f665b53e3e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6aa6dd8-e2b1-4efb-adcc-26fd8281cb53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52a61f41-eaf8-42b7-8579-41e87e3060de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6aa6dd8-e2b1-4efb-adcc-26fd8281cb53",
                    "LayerId": "a114fcab-3b29-434e-a43b-ff3a56a7e634"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "a114fcab-3b29-434e-a43b-ff3a56a7e634",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88581923-9133-416e-9f45-78ddc7c53296",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}