{
    "id": "fb726328-1c50-422e-9bb5-a994896c3739",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_enemy_ko",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f896fb60-ed9e-40db-8bc4-ca2ee1a3aaca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb726328-1c50-422e-9bb5-a994896c3739",
            "compositeImage": {
                "id": "12acc3a0-f695-4499-ad58-f5deca7c0181",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f896fb60-ed9e-40db-8bc4-ca2ee1a3aaca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d83cbcf-ffc6-42d3-a022-75764c63ab09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f896fb60-ed9e-40db-8bc4-ca2ee1a3aaca",
                    "LayerId": "32ad5bf6-e046-4af5-a039-1a5f1d755d8b"
                }
            ]
        },
        {
            "id": "da0214d1-5000-442a-bda8-cfaa77663eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb726328-1c50-422e-9bb5-a994896c3739",
            "compositeImage": {
                "id": "1a773650-0f82-4539-891c-badf8a67445d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da0214d1-5000-442a-bda8-cfaa77663eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99daad43-7621-40bf-a626-1b85b4c9d893",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da0214d1-5000-442a-bda8-cfaa77663eef",
                    "LayerId": "32ad5bf6-e046-4af5-a039-1a5f1d755d8b"
                }
            ]
        },
        {
            "id": "57e1c5e0-2a86-4782-a1f4-e0ac32a2cdb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb726328-1c50-422e-9bb5-a994896c3739",
            "compositeImage": {
                "id": "a6a32aaf-f6c6-4fb7-8ed9-0cb39ed3ed4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57e1c5e0-2a86-4782-a1f4-e0ac32a2cdb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1f57d5d-4258-4750-80bd-a8a4ab57ce69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57e1c5e0-2a86-4782-a1f4-e0ac32a2cdb4",
                    "LayerId": "32ad5bf6-e046-4af5-a039-1a5f1d755d8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "32ad5bf6-e046-4af5-a039-1a5f1d755d8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb726328-1c50-422e-9bb5-a994896c3739",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 8
}