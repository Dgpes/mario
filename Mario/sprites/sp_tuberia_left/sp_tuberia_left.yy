{
    "id": "09c9bc8d-62dc-48bd-a99c-64c7dc4a3dff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_tuberia_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b21d8fa1-9fa2-45ae-8735-3d7121b445f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09c9bc8d-62dc-48bd-a99c-64c7dc4a3dff",
            "compositeImage": {
                "id": "60bc1ca1-f69b-447e-9f12-f12eaa7f99b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b21d8fa1-9fa2-45ae-8735-3d7121b445f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ebe9bcb-98e9-4a7f-897b-de054270e854",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b21d8fa1-9fa2-45ae-8735-3d7121b445f7",
                    "LayerId": "3642ab2a-5f4c-4709-bd54-4d030a28c9e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3642ab2a-5f4c-4709-bd54-4d030a28c9e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09c9bc8d-62dc-48bd-a99c-64c7dc4a3dff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 16
}