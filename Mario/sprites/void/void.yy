{
    "id": "a0007c54-a8e7-4fbd-a2bc-bb906eff45ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "void",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2da1eb0-b2f8-4b98-b7b0-8ea8202103f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0007c54-a8e7-4fbd-a2bc-bb906eff45ee",
            "compositeImage": {
                "id": "ae42c588-c5ff-400f-93ba-6e7cc550d480",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2da1eb0-b2f8-4b98-b7b0-8ea8202103f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbf6b9d7-7cc1-4397-9628-58773cbc975a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2da1eb0-b2f8-4b98-b7b0-8ea8202103f6",
                    "LayerId": "6c299d06-db9d-4ab9-8d58-d1b5391a9952"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6c299d06-db9d-4ab9-8d58-d1b5391a9952",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0007c54-a8e7-4fbd-a2bc-bb906eff45ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}