{
    "id": "48d2f83d-0404-4e8a-9b26-2776757bb137",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_enemy_walking_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3422481f-f791-4dab-a27a-7b816f75cccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48d2f83d-0404-4e8a-9b26-2776757bb137",
            "compositeImage": {
                "id": "051e8996-de03-46fe-8405-7f7dacc3e047",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3422481f-f791-4dab-a27a-7b816f75cccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff33940e-30e9-4eeb-aff3-3dd16eab74a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3422481f-f791-4dab-a27a-7b816f75cccb",
                    "LayerId": "e6b24d8a-1cf2-461e-826a-4e471d5016b5"
                }
            ]
        },
        {
            "id": "a8f3da1d-5baa-45ff-8ee3-2a3f5fb72d0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48d2f83d-0404-4e8a-9b26-2776757bb137",
            "compositeImage": {
                "id": "c541f8b6-7f8f-4209-82b1-1e8c88205e7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f3da1d-5baa-45ff-8ee3-2a3f5fb72d0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3b4516e-4428-4ed4-8255-dfc1a62f0b5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f3da1d-5baa-45ff-8ee3-2a3f5fb72d0c",
                    "LayerId": "e6b24d8a-1cf2-461e-826a-4e471d5016b5"
                }
            ]
        },
        {
            "id": "bbe54f08-e56d-4a8d-8ab5-d26415b559e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48d2f83d-0404-4e8a-9b26-2776757bb137",
            "compositeImage": {
                "id": "500bd49a-ba28-41c0-9e65-92fbf84bccac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbe54f08-e56d-4a8d-8ab5-d26415b559e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ec7030c-9ea0-42cf-b78e-3cd6731ebf8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbe54f08-e56d-4a8d-8ab5-d26415b559e9",
                    "LayerId": "e6b24d8a-1cf2-461e-826a-4e471d5016b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e6b24d8a-1cf2-461e-826a-4e471d5016b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48d2f83d-0404-4e8a-9b26-2776757bb137",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}