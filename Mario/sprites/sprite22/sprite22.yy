{
    "id": "28ea2e7d-9188-4272-af90-10a4d8814141",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite22",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbb82ab3-5858-4d68-af08-70f26f2b6d68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28ea2e7d-9188-4272-af90-10a4d8814141",
            "compositeImage": {
                "id": "23ca4a0b-0bc3-4249-95dd-433f93f92265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbb82ab3-5858-4d68-af08-70f26f2b6d68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e893d2f5-cd9b-4a48-b2dc-b0e53901b09a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbb82ab3-5858-4d68-af08-70f26f2b6d68",
                    "LayerId": "b782b0a9-1d56-4768-b7e5-7e242bba938c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b782b0a9-1d56-4768-b7e5-7e242bba938c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28ea2e7d-9188-4272-af90-10a4d8814141",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}