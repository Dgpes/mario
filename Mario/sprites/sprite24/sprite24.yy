{
    "id": "f59de363-e647-4b40-9bdc-3ffc078f9d93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite24",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5101439d-9e4f-4209-8bf4-343b757b049e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f59de363-e647-4b40-9bdc-3ffc078f9d93",
            "compositeImage": {
                "id": "5005d5a4-e178-4424-b354-004034fde787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5101439d-9e4f-4209-8bf4-343b757b049e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "558d0560-6114-4350-aa99-e44021f9b7ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5101439d-9e4f-4209-8bf4-343b757b049e",
                    "LayerId": "7727620e-8f9e-4959-b634-3e1a31a2d093"
                }
            ]
        },
        {
            "id": "b9407f66-bf47-429b-aa04-8b43faef5fab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f59de363-e647-4b40-9bdc-3ffc078f9d93",
            "compositeImage": {
                "id": "dc04f72f-8f5b-4aa3-9d4c-901185d2f7a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9407f66-bf47-429b-aa04-8b43faef5fab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97cdd1ea-8b8f-463f-a1b4-5a07b360289b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9407f66-bf47-429b-aa04-8b43faef5fab",
                    "LayerId": "7727620e-8f9e-4959-b634-3e1a31a2d093"
                }
            ]
        },
        {
            "id": "f8840aa3-4fd1-405f-845e-3c8909cd41c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f59de363-e647-4b40-9bdc-3ffc078f9d93",
            "compositeImage": {
                "id": "f55023cb-e87c-4395-8d66-9342b17bcccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8840aa3-4fd1-405f-845e-3c8909cd41c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2609bf0-77f4-48d8-ad01-9ee803e5f1a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8840aa3-4fd1-405f-845e-3c8909cd41c2",
                    "LayerId": "7727620e-8f9e-4959-b634-3e1a31a2d093"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7727620e-8f9e-4959-b634-3e1a31a2d093",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f59de363-e647-4b40-9bdc-3ffc078f9d93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}