{
    "id": "c83f6efe-57d4-44fd-a1b3-16a895cc3fe3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_jump_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "482126a9-abe0-41ce-989c-70712b79ad55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c83f6efe-57d4-44fd-a1b3-16a895cc3fe3",
            "compositeImage": {
                "id": "a9a3db1d-ea3c-4865-9bb9-3ec7d4b0587a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "482126a9-abe0-41ce-989c-70712b79ad55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "733f933d-713c-49e2-8428-f6a92929888f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "482126a9-abe0-41ce-989c-70712b79ad55",
                    "LayerId": "78df03a1-b98e-41cf-bf31-323c05aff0cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "78df03a1-b98e-41cf-bf31-323c05aff0cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c83f6efe-57d4-44fd-a1b3-16a895cc3fe3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 11
}