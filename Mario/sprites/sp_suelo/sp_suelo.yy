{
    "id": "6c4e179e-f5da-4062-ba3b-f955c792f4e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_suelo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b64f2fed-74ee-400b-970c-048c8922f910",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c4e179e-f5da-4062-ba3b-f955c792f4e6",
            "compositeImage": {
                "id": "b9718205-9e2e-4b8f-97a7-62be9edfc3b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b64f2fed-74ee-400b-970c-048c8922f910",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1475f6bc-b42b-41e3-bbff-31bcb9dada39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b64f2fed-74ee-400b-970c-048c8922f910",
                    "LayerId": "ab6771c4-e499-42b6-9e5f-7b9b74c7fdad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ab6771c4-e499-42b6-9e5f-7b9b74c7fdad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c4e179e-f5da-4062-ba3b-f955c792f4e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}