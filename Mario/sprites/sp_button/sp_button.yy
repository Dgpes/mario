{
    "id": "2f221846-87b2-430f-8f81-6e7314f221c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sp_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 139,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32102626-2083-4814-a1ef-2efe9bf4ebe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f221846-87b2-430f-8f81-6e7314f221c8",
            "compositeImage": {
                "id": "28ce0a0c-2caa-4a8c-8718-34ef5790bea9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32102626-2083-4814-a1ef-2efe9bf4ebe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a02924be-6752-4462-9208-a2729e5c8e96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32102626-2083-4814-a1ef-2efe9bf4ebe9",
                    "LayerId": "fbc25ec2-c783-4093-a89e-2c85d744a2e5"
                }
            ]
        },
        {
            "id": "558e3dab-4ad6-4bb2-9a37-8c3d5b3c19fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f221846-87b2-430f-8f81-6e7314f221c8",
            "compositeImage": {
                "id": "a337828d-54d4-4e55-a41a-09b6f78b6991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "558e3dab-4ad6-4bb2-9a37-8c3d5b3c19fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c585c10-b4de-4391-9153-aea17c87495f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "558e3dab-4ad6-4bb2-9a37-8c3d5b3c19fc",
                    "LayerId": "fbc25ec2-c783-4093-a89e-2c85d744a2e5"
                }
            ]
        },
        {
            "id": "7a19cc30-7e61-4253-a82c-ec43eb1b79ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f221846-87b2-430f-8f81-6e7314f221c8",
            "compositeImage": {
                "id": "cb669db3-e5f6-4453-af50-63be643884a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a19cc30-7e61-4253-a82c-ec43eb1b79ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64444bcc-be74-4bca-b505-4690971a01b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a19cc30-7e61-4253-a82c-ec43eb1b79ab",
                    "LayerId": "fbc25ec2-c783-4093-a89e-2c85d744a2e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fbc25ec2-c783-4093-a89e-2c85d744a2e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f221846-87b2-430f-8f81-6e7314f221c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 140,
    "xorig": 0,
    "yorig": 0
}