{
    "id": "60a49567-7b12-4910-8453-17f363fdcb8f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite25",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 253,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b79450e-9d01-4a89-ac19-d6904d6688a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60a49567-7b12-4910-8453-17f363fdcb8f",
            "compositeImage": {
                "id": "b2d50806-e7f3-4b2f-b294-9a72fea865ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b79450e-9d01-4a89-ac19-d6904d6688a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f562d94c-9a0e-4cc4-aa26-7d4d0af98631",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b79450e-9d01-4a89-ac19-d6904d6688a7",
                    "LayerId": "1381941f-6bd6-41d8-92a4-9009163f1a3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "1381941f-6bd6-41d8-92a4-9009163f1a3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60a49567-7b12-4910-8453-17f363fdcb8f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 254,
    "xorig": 0,
    "yorig": 0
}